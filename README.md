The goal of the research was to teach convolutional neural network to predict if animal on the photo is cat or dog. Training data set contained 4000 pictures of cats and the same number of dogs images. Test set contained accordingly 1000 pictures of cats and 1000 pictures of dogs. 
It has been checked how train and test accuracies change when changing some parameters: dropout, output dimensions, number of feature detectors, image size, convolutional layers amount. 

