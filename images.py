#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 23 13:58:13 2018

@author: zz
"""
from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Activation
import numpy as np
import matplotlib.pyplot as plt
import cv2
cat = cv2.imread('/dataset/training_set/cats/cat.14.jpg')


def animal_printer(cat, model):
    batch = np.expand_dims(cat, axis=0)
    conv = model.predict(batch)
    conv = np.squeeze(conv,axis=0)
    conv = conv.reshape(conv.shape[:2])
    plt.imshow(conv)

    
classifier = Sequential()
# Step 1 - Convolution
classifier.add(Conv2D(1, (3, 3), input_shape= cat.shape))
animal_printer(cat, classifier)
plt.savefig('conv.png')
plt.show()
plt.clf()
plt.cla()
plt.close()

classifier = Sequential()
#Activation
classifier.add(Conv2D(1, (3, 3), input_shape= cat.shape))
classifier.add(Activation('relu'))
animal_printer(cat, classifier)
plt.savefig('activ.png')
plt.show()
plt.clf()
plt.cla()
plt.close()

# Step 3 - Pooling
classifier = Sequential()
classifier.add(Conv2D(1, (3, 3), input_shape= cat.shape))
classifier.add(Activation('relu'))
classifier.add(MaxPooling2D(pool_size = (2, 2)))
animal_printer(cat, classifier)
plt.savefig('pool.png')
plt.show()