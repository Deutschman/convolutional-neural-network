#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 18 19:25:03 2018

@author: zz
"""
import re
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('ggplot')


files = ['loss_history_2.log', 'loss_history_4.log', 'loss_history_3.log', 'loss_history.log']
names = ['3_64', '3_128', '2_128', '2_64']
d={}
for idx,file in enumerate(files):
    numbers = [re.findall(r'([\d\.]*)',line) for line in open(file, 'r')]
    flat_list = [item for sublist in numbers for item in sublist]
    numbers = np.asarray(list(filter(None, flat_list)))
    numbers = np.delete(numbers, 0)
    d["string{0}".format(idx)] = np.reshape(numbers, (-1, 3))

val_3_64 = d.get('string0').astype(float)
val_3_128 = d.get('string1').astype(float)
val_2_128 = d.get('string2').astype(float)
val_2_64 = d.get('string3').astype(float)

steps = np.linspace(1,90,90, dtype = int, endpoint=True)

fig = plt.figure(figsize=(10, 6))
ax = fig.add_subplot(111)

lines = plt.plot(np.array(val_3_64[:,0]), np.array(val_3_64[:,2]), 
                 np.array(val_2_128[:,0]), np.array(val_2_128[:,2]), 
                 np.array(val_3_128[:,0]), np.array(val_3_128[:,2]),
                 np.array(val_2_64[:,0]), np.array(val_2_64[:,2]))
l1, l2, l3, l4 = lines
plt.setp(l1, linewidth=1, color='r', linestyle='-', label = '3 layers, 64x64 size') # set function 1 linestyle
plt.setp(l2, linewidth=1, color='g', linestyle='-', label = '3 layers, 128x128 size') # set function 2 linestyle
plt.setp(l3, linewidth=1, color='g', linestyle='--', label = '2 layers, 128x128 size') # set function 2 linestyle
plt.setp(l4, linewidth=1, color='r', linestyle='--', label = '2 layers, 64x64 size')
ax.yaxis.set_major_locator(plt.MaxNLocator(8))

plt.ylim(0.4,1)
plt.xlim(-1,90)
plt.title('Validation data accuracy')
plt.xlabel('number of epoch')
plt.ylabel('accuracy')
plt.legend()

plt.tight_layout()
plt.savefig('validation_accuracy.png')
plt.show()



fig = plt.figure(figsize=(10, 6))
ax = fig.add_subplot(111)

lines = plt.plot(np.array(val_3_64[:,0]), np.array(val_3_64[:,1]), 
                 np.array(val_2_128[:,0]), np.array(val_2_128[:,1]), 
                 np.array(val_3_128[:,0]), np.array(val_3_128[:,1]),
                 np.array(val_2_64[:,0]), np.array(val_2_64[:,1]))
l1, l2, l3, l4 = lines
plt.setp(l1, linewidth=1, color='r', linestyle='-', label = '3 layers, 64x64 size') # set function 1 linestyle
plt.setp(l2, linewidth=1, color='g', linestyle='-', label = '3 layers, 128x128 size') # set function 2 linestyle
plt.setp(l3, linewidth=1, color='g', linestyle='--', label = '2 layers, 128x128 size') # set function 2 linestyle
plt.setp(l4, linewidth=1, color='r', linestyle='--', label = '2 layers, 64x64 size') # set function 2 linestyle
ax.yaxis.set_major_locator(plt.MaxNLocator(8))
plt.ylim(0.4,1)
plt.xlim(-1,90)
plt.title('Train data accuracy')
plt.xlabel('number of epoch')
plt.ylabel('accuracy')
plt.legend()
plt.tight_layout()
plt.savefig('train_accuracy.png')
plt.show()

#------------------------------------dropout or not------------------------------


files = ['loss_history_2.log', 'loss_with_dropout.log']
#names = ['3_64', '3_128', '2_128', '2_64']
d={}
for idx,file in enumerate(files):
    numbers = [re.findall(r'([\d\.]*)',line) for line in open(file, 'r')]
    flat_list = [item for sublist in numbers for item in sublist]
    numbers = np.asarray(list(filter(None, flat_list)))
    numbers = np.delete(numbers, 0)
    d["string{0}".format(idx)] = np.reshape(numbers, (-1, 3))

val_3_64 = d.get('string0').astype(float)
val_drop = d.get('string1').astype(float)

steps = np.linspace(1,90,90, dtype = int, endpoint=True)

fig = plt.figure(figsize=(10, 6))
ax = fig.add_subplot(111)

lines = plt.plot(np.array(val_3_64[:,0]), np.array(val_3_64[:,2]), 
                 np.array(val_drop[:,0]), np.array(val_drop[:,2]))
l1, l2 = lines
plt.setp(l1, linewidth=1, color='r', linestyle='-', label = '3 layers, 64x64 size') # set function 1 linestyle
plt.setp(l2, linewidth=1, color='g', linestyle='-', label = '3 layers, 64x64 size with dropout') # set function 2 linestyle

ax.yaxis.set_major_locator(plt.MaxNLocator(8))

plt.ylim(0.4,1)
plt.xlim(-1,90)
plt.title('Validation data accuracy')
plt.xlabel('number of epoch')
plt.ylabel('accuracy')
plt.legend()

plt.tight_layout()
plt.savefig('validation_accuracy_dropout.png')
plt.show()



fig = plt.figure(figsize=(10, 6))
ax = fig.add_subplot(111)

lines = plt.plot(np.array(val_3_64[:,0]), np.array(val_3_64[:,1]), 
                 np.array(val_drop[:,0]), np.array(val_drop[:,1]))
l1, l2 = lines
plt.setp(l1, linewidth=1, color='r', linestyle='-', label = '3 layers, 64x64 size') # set function 1 linestyle
plt.setp(l2, linewidth=1, color='g', linestyle='-', label = '3 layers, 64x64 size with dropout') # set function 2 linestyle

ax.yaxis.set_major_locator(plt.MaxNLocator(8))
plt.ylim(0.4,1)
plt.xlim(-1,90)
plt.title('Train data accuracy')
plt.xlabel('number of epoch')
plt.ylabel('accuracy')
plt.legend()
plt.tight_layout()
plt.savefig('train_accuracy_dropout.png')
plt.show()

#------------------------2 outputs, 1 output


files = ['loss_history_2.log', 'loss_two_outs.log']
#names = ['3_64', '3_128', '2_128', '2_64']
d={}
for idx,file in enumerate(files):
    numbers = [re.findall(r'([\d\.]*)',line) for line in open(file, 'r')]
    flat_list = [item for sublist in numbers for item in sublist]
    numbers = np.asarray(list(filter(None, flat_list)))
    numbers = np.delete(numbers, 0)
    d["string{0}".format(idx)] = np.reshape(numbers, (-1, 3))

val_3_64 = d.get('string0').astype(float)
val_two_outs = d.get('string1').astype(float)

steps = np.linspace(1,90,90, dtype = int, endpoint=True)

fig = plt.figure(figsize=(10, 6))
ax = fig.add_subplot(111)

lines = plt.plot(np.array(val_3_64[:,0]), np.array(val_3_64[:,2]), 
                 np.array(val_two_outs[:,0]), np.array(val_two_outs[:,2]))
l1, l2 = lines
plt.setp(l1, linewidth=1, color='r', linestyle='-', label = '1 output') # set function 1 linestyle
plt.setp(l2, linewidth=1, color='g', linestyle='-', label = '2 outputs') # set function 2 linestyle

ax.yaxis.set_major_locator(plt.MaxNLocator(8))

plt.ylim(0.4,1)
plt.xlim(-1,90)
plt.title('Validation data accuracy')
plt.xlabel('number of epoch')
plt.ylabel('accuracy')
plt.legend()

plt.tight_layout()
plt.savefig('validation_accuracy_2_outs.png')
plt.show()



fig = plt.figure(figsize=(10, 6))
ax = fig.add_subplot(111)

lines = plt.plot(np.array(val_3_64[:,0]), np.array(val_3_64[:,1]), 
                 np.array(val_two_outs[:,0]), np.array(val_two_outs[:,1]))
l1, l2 = lines
plt.setp(l1, linewidth=1, color='r', linestyle='-', label = '1 output') # set function 1 linestyle
plt.setp(l2, linewidth=1, color='g', linestyle='-', label = '2 outputs') # set function 2 linestyle

ax.yaxis.set_major_locator(plt.MaxNLocator(8))
plt.ylim(0.4,1)
plt.xlim(-1,90)
plt.title('Train data accuracy')
plt.xlabel('number of epoch')
plt.ylabel('accuracy')
plt.legend()
plt.tight_layout()
plt.savefig('train_accuracy_dropout_2_outs.png')
plt.show()